#Encoding: utf-8

require 'csv'
require 'net/http'
require 'json'

def removing(temp)
    temp.gsub!('(','')
    temp.gsub!(')','')
    temp.gsub!('-','')
    temp.gsub!(' ','')
    temp.gsub!(/\D/,'')
    temp
end

all_items = []
count = 0

File.open("log.txt", "w") do |f|  
    CSV.foreach("RJ_Contacts_31284.csv", {:col_sep => ",", :headers=>:first_row, encoding: "ISO8859-1"}) do |row|
    
=begin
    home_street = Current Address
=end
        
        mobile_phone = row["Mobile Phone"]
        original_mobile_phone = mobile_phone
        mobile_phone = !mobile_phone.nil? ? removing(mobile_phone) : "" 
        
        car_phone = row["Car Phone"]
        original_car_phone = car_phone
        car_phone = !car_phone.nil? ? removing(car_phone) : ""

        other_fax = row["Other Fax"]
        original_other_fax = other_fax
        other_fax = !other_fax.nil? ? removing(other_fax) : ""

        first_name = ""
        last_name = ""
        first_name = row["First Name"]
        first_name.strip! if !first_name.nil? 
        last_name = row["Last Name"]
        last_name.strip! if !last_name.nil?
        

        
        name = (first_name.nil? ? "" : first_name) + " " + (last_name.nil? ? "" : last_name)
        anniversary = row["Anniversary"]
        birthday = row["Birthday"]
        categories = row["Categories"]
        email = row["E-mail Address"]
        notes = row["Notes"]
        referred_by = row["Referred By"]
        spouse = row["Spouse"]
        last_contact_date = row["Last Contact Date"]
        last_modified_date = row["Last Modified Date"]
        created_date = row["Created Date"]

        telephones = []
        telephones.push({value: mobile_phone, label: "Mobile", primary: true})
        telephones.push({value: car_phone, label: "Car Phone"}) if car_phone != ""
        telephones.push({value: other_fax, label: "Other Fax"}) if other_fax != ""

        url = "https://api.pipedrive.com/v1/persons?api_token=e362da30911c20bd8b625d339f0c9412603fa4f4"
        uri = URI.parse(url)
        request = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')

        body = {
            'name': name,
            'phone': telephones,
            'email': email,
            'label': 'Customer',
            'cbf63ca1548feb1b66ff1842afecc42678026520': anniversary,
            '2697aafd23802730968901753313b0a25f91ca56': birthday,
            'be3ee70c90db27a31d69b59ca6331a5c98b2eddb': categories,
            '5827f5295c3d16c0bba01ad567eeb570ed174bdf': notes,
            'd612de1f37b8b7411773b3874dd8b2947859c082': referred_by,
            'a1b2ee39c00c2f1a18b02d0b89388b08d2b25ca6': spouse,
            '0a5b6247c3988a68f8dbb1b0eb57c1c10b8f9f51': last_contact_date,
            'd05346b8593d79335c86ae71847bedbdddb755ea': last_modified_date,
            '8d69cb8d19c740648f58143a7443afad31ede091': created_date,
            '5827f5295c3d16c0bba01ad567eeb570ed174bdf': notes,
            '57a82a4f67fd9ac02eaa007a0b1d15cd1f345a08': original_mobile_phone,
            '90a5f4bcb78f49aa4a81efdf4595ce40593425b6': original_car_phone,
            'dbddf7eaf377cb23235b71b8c0f3007da0c06a48': original_other_fax 

        }

        request.body = JSON.dump(body)

        req_options = {
            use_ssl: uri.scheme == "https",
        }

        response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
            http.request(request)
        end

        if response.is_a?(Net::HTTPSuccess)
            p "OK"
            f.write "OK\n"
        else
            p "ERROR"
            f.write "ERROR - #{response.body}\n"
            p response
        end
    end
end

=begin

- El primer recordatorio tiene que hacerse a los 6 meses de haber cerrado la transaccion. Para refinanciar. (Refinance). When the deal is won it's going to happen.
- El segundo recordatorio tiene que hacerse a los 12 meses de habserse cerrado la transaccion. Aniversario de Cierre de la Transacción. 
    Aniversario de compra de la casa. When a deal is won, it's going to happen.



=end