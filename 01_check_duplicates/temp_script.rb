require 'net/http'
require 'json'

name = "Danilo Garcia"
email = {value: "danilogarcia024@gmail.com", label: "home", primary: true}
telephone_1 = {value: "3003146955", label: "Mobile", primary: true}
telephone_2 = {value: "3461975", label: "Car Phone"}
telephone_3 = {value: "123456", label: "Other Fax"}
telephones = [telephone_1, telephone_2, telephone_3]
emails = [email]
anniversary = "1/28/2021"
birthday = "8/5/2021"
categories = "Example 1"

url = "https://api.pipedrive.com/v1/persons?api_token=e362da30911c20bd8b625d339f0c9412603fa4f4"
uri = URI.parse(url)
request = Net::HTTP::Post.new(uri, 'Content-Type' => 'application/json')

body = {
    'name': name,
    'email': emails,
    'phone': telephones,
    'email': email,
    'label': 'Customer',
    'cbf63ca1548feb1b66ff1842afecc42678026520': anniversary,
    '2697aafd23802730968901753313b0a25f91ca56': birthday,
    'be3ee70c90db27a31d69b59ca6331a5c98b2eddb': categories,
    '5827f5295c3d16c0bba01ad567eeb570ed174bdf': notes,
    'd612de1f37b8b7411773b3874dd8b2947859c082': referred_by,
    'a1b2ee39c00c2f1a18b02d0b89388b08d2b25ca6': spouse,
    'a1b2ee39c00c2f1a18b02d0b89388b08d2b25ca6': last_contact_date,
    'd05346b8593d79335c86ae71847bedbdddb755ea': last_modified_date,
    'd05346b8593d79335c86ae71847bedbdddb755ea': created_date,
    '5827f5295c3d16c0bba01ad567eeb570ed174bdf': notes,
    '57a82a4f67fd9ac02eaa007a0b1d15cd1f345a08': original_mobile_phone,
    '90a5f4bcb78f49aa4a81efdf4595ce40593425b6': original_car_phone,
    'dbddf7eaf377cb23235b71b8c0f3007da0c06a48': original_other_fax 

}

request.body = JSON.dump(body)

req_options = {
    use_ssl: uri.scheme == "https",
}

response = Net::HTTP.start(uri.hostname, uri.port, req_options) do |http|
    http.request(request)
end

if response.is_a?(Net::HTTPSuccess)
    p "OK"
else
    p response
end